package com.br.ubots.bot.response.send;

import com.br.ubots.bot.httpbodyformat.send.facebook.*;
import com.google.gson.Gson;
import org.springframework.stereotype.Component;

@Component
public class FacebookMessageBuilder {

    private Gson gson;

    public FacebookMessageBuilder(Gson gson) {
        this.gson = gson;
    }

    public String formatToSend(String response, String id) {
        Message message = botMessage(response);
        Recipient recipient = messageRecipient(id);
        Body body = new Body(message, "RESPONSE", recipient);
        return gson.toJson(body);
    }

    private Message botMessage(String response) {
        Message botMessage = new Message();
        botMessage.setText(response);
        return botMessage;
    }

    private Recipient messageRecipient(String id) {
        Recipient recipient = new Recipient();
        recipient.setId(id);
        return recipient;
    }
}