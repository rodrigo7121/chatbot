package com.br.ubots.bot.response.send.API;

import com.br.ubots.bot.httpbodyformat.received.dialogflow.DialogFlowMessage;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

public class DialogflowAPI {

    private RestTemplate restTemplate;
    private String url;
    private String token;

    public DialogflowAPI(RestTemplate restTemplate, String url, String token) {
        this.restTemplate = restTemplate;
        this.url = url;
        this.token = token;
    }

    public DialogFlowMessage getReceivedMessage(String bodyMessage) {
        URI uri = URI.create(url);
        HttpHeaders header = RestAttributes.buildDialogFlowHttpHeader(token);
        HttpEntity<String> body = RestAttributes.buildHttpEntity(bodyMessage, header);
        ResponseEntity<DialogFlowMessage> responseEntity = restTemplate.postForEntity(uri, body, DialogFlowMessage.class);
        return responseEntity.getBody();
    }
}
