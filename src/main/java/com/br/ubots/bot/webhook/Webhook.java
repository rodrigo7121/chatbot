package com.br.ubots.bot.webhook;

public class Webhook {

    private final String challenge;

    public Webhook(String challenge) {
        this.challenge = challenge;
    }

    public String getChallenge() {
        return challenge;
    }
}
