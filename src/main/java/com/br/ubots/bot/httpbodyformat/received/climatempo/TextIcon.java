package com.br.ubots.bot.httpbodyformat.received.climatempo;

public class TextIcon {

    private Text text;

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }
}
