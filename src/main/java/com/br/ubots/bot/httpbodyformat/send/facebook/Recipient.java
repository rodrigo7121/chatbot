package com.br.ubots.bot.httpbodyformat.send.facebook;

public class Recipient {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}