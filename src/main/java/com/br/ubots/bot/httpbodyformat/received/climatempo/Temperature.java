package com.br.ubots.bot.httpbodyformat.received.climatempo;

public class Temperature {

    private int min;
    private int max;

    public int getMedia() {
        return (min + max)/2;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setMax(int max) {
        this.max = max;
    }
}
