package com.br.ubots.bot.httpbodyformat.received.facebook;

public class FacebookMessageBody {

    public Entry[] getEntry() {
        return entry;
    }

    public void setEntry(Entry[] entry) {
        this.entry = entry;
    }

    private Entry[] entry;

}
